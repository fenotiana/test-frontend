const tailwindForms = require("@tailwindcss/forms");

module.exports = {
  // purge: ["./src/**/*.{vue}", "./src/**/**/*.{vue}", "./src/**/**/**/*.{vue}"],
  purge: {
    enable: true,
    content: [
      "./src/views/components/*.vue",
      "./src/views/components/**/*.vue",
      "./src/views/screens/*.vue",
      "./src/views/screens/**/*.vue",
    ],
  },
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      display: ["hover", "focus"],
      colors: {
        green: { 400: "#82BC02" },
      },
    },
  },
  variants: {
    extend: {
      margin: ["first"],
      ringWidth: ["hover", "active"],
      opacity: ["disabled"],
      cursor: ["hover", "focus"],
    },
  },
  plugins: [tailwindForms],
};
