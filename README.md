# test-frontend

## Installation des dépendances

```
yarn install
```

### Lancer le serveur dev

```
yarn serve
```

Entrez http://localhost:8080 sur votre navigateur

### Lancer build pour production

```
yarn build
```

### Demo
Pour voir la démo,
[Cliquez ici](https://lucid-mccarthy-a324fe.netlify.app)
