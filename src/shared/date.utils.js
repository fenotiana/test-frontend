import * as dayjs from "dayjs";

const format = "YYYY-MM-DDTHH:mm:ss.sssZ";

export const formatDateWithHour = (dateHour) =>
  dayjs(dateHour, format).format("YYYY-MM-DD, à HH[h]mm");

export const formatDate = (date) =>
  date
    ? dayjs(date, format).format("YYYY-MM-DD")
    : dayjs().format("DD/MM/YYYY");

export const formatDateToHour = (date) =>
  date ? dayjs(date, format).format("HH:mm") : dayjs().format("HH:mm");

export const formatDateToTimezone = (date) =>
  date ? dayjs(date).format(format) : dayjs().format(format);
