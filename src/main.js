import { createApp } from "vue";

import App from "./views/screens/App.vue";
import store from "./store";
import "./tailwind.out.css";

const app = createApp(App);

app.use(store);

app.mount("#app");
