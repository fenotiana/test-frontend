import { createStore } from "vuex";
import event from "./modules/event";

const isDev = process.env.NODE_ENV !== "production";

export default createStore({
  modules: { event },
  strict: isDev,
});
