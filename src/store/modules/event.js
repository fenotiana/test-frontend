import { getEvents } from "../../mock/data";

const SET_EVENTS = "setEvents";
const SET_SELECTED_EVENT = "setSelectedEvent";
const UPDATE_EVENT = "updateEvent";

const state = () => ({
  events: [],
  selectedEvent: null,
});

const getters = {
  events: (state) => state.events,
  selectedEvent: (state) => state.selectedEvent,
};

const mutations = {
  [SET_EVENTS]: (state, { events }) => (state.events = events),
  [SET_SELECTED_EVENT]: (state, { selectedEvent }) =>
    (state.selectedEvent = selectedEvent),
  [UPDATE_EVENT]: (state, payload) => {
    const { id } = payload;
    const found = state.events.find((event) => event.id === id);
    Object.assign(found, payload);

    console.log("Mise à jour de l'information concernant l'évènement");
  },
};

const actions = {
  getEvents: ({ commit }) => {
    console.log("Récupérer la liste des évènements d'une REST API par exemple");
    commit(SET_EVENTS, { events: getEvents() });
  },
};

export default {
  namespaced: true,
  getters,
  actions,
  state,
  mutations,
};
